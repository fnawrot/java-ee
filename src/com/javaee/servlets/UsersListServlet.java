package com.javaee.servlets;

import com.javaee.UserService;
import com.javaee.model.User;
import com.javaee.dao.UsersDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users")
public class UsersListServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        resp.setContentType("text/html;charset=UTF-8");

        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "    <head>\n" +
                "        <meta charset=\"UTF-8\">\n" +
                "        <title>Użytkownicy</title>\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <h1>\n" +
                "            Zarejestrowani użytkownicy\n" +
                "        </h1>\n" +
                "\n" +
                "        <table>\n<tr>\n" +
                "<th><b>Login</b></th>\n" +
                "<th><b>Imię</b></th>\n" +
                "<th><b>Nazwisko</b></th>\n" +
                "<th><b>Rok urodzenia</b></th>\n" +
                "<th><b>Miasto</b></th>\n" +
                "</tr>\n");

        for (User user : userService.getAllUsers()) {
            resp.getWriter().write("<tr>");
            resp.getWriter().write("<td>" + user.getLogin() + "</td>");
            resp.getWriter().write("<td>" + user.getName() + "</td>");
            resp.getWriter().write("<td>" + user.getSurname() + "</td>");
            resp.getWriter().write("<td>" + user.getBirthYear() + "</td>");
            resp.getWriter().write("<td>" + user.getCity() + "</td>");
            resp.getWriter().write("</tr>");
        }

        resp.getWriter().write(
                "\n" +
                        "        </table>\n" +
                        "    </body>\n" +
                        "</html>");
    }
}
