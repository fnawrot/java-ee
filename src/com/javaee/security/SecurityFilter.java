package com.javaee.security;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");

        String username = (String) req.getSession().getAttribute("username");
        String uri = req.getRequestURI();
        if (username != null || uri.endsWith("login") || uri.endsWith("login.html")
                || uri.endsWith(".css") || uri.endsWith(".js") || uri.endsWith(".png")
                || uri.endsWith("register.html") || uri.endsWith("register")) {
            chain.doFilter(req, res);
        } else {
            res.sendRedirect(res.encodeRedirectURL(req.getContextPath() + "/login.html"));
        }
    }
}
