package com.javaee.login;

import com.javaee.UserService;
import com.javaee.model.User;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User userByLogin = userService.findUserByLogin(username);

        if (userByLogin != null && userByLogin.getPassword().equals(password)) {
            req.getSession().setAttribute("username", username);
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/homepage.jsp"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/login.html"));
        }
    }
}
