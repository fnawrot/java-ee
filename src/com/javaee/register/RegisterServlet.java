package com.javaee.register;

import com.javaee.TotalSingleton;
import com.javaee.UserService;
import com.javaee.model.User;
import com.javaee.dao.UsersDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String city = req.getParameter("city");
        String birthYear = req.getParameter("birthYear");

        User newUser = new User(username, password, name, surname, city, Integer.parseInt(birthYear));


        userService.registerUser(newUser);

        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/login.html"));
    }
}
