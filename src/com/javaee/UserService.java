package com.javaee;

import com.javaee.dao.UsersDAO;
import com.javaee.model.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class UserService {
    @EJB
    private UsersDAO usersDAO;

    public List<User> getAllUsers() {
        return usersDAO.getAllUsers();
    }

    public void registerUser(User user) {
        usersDAO.registerUser(user);
    }

    public void deleteUserById(String id) {
        usersDAO.deleteUserById(id);
    }

    public User findUserByLogin(String login) {
        return usersDAO.findUserByLogin(login);
    }


}
