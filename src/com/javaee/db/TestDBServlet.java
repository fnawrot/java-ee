package com.javaee.db;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@WebServlet("/testDB")
public class TestDBServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/jee_project?user=postgres&password=root&ssl=false";
            Connection conn = DriverManager.getConnection(url);

            resp.getWriter().write("Connection succesfull");
        } catch (Exception e) {
            resp.getWriter().write("Connection FAILED :( " + e.toString());
        }

    }
}