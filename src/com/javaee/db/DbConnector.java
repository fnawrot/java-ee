package com.javaee.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnector {

    public static Connection createConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/jee_project?user=postgres&password=root&ssl=false";
            return DriverManager.getConnection(url);

        } catch (Exception e) {
            return null;
        }
    }
}
