package com.javaee.model;

import java.util.UUID;

public class EntryCategory {
    private UUID entryId;
    private UUID categoryId;

    public EntryCategory(UUID entryId, UUID categoryId) {
        this.entryId = entryId;
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "EntryCategory{" +
                "entryId=" + entryId +
                ", categoryId=" + categoryId +
                '}';
    }

    public UUID getEntryId() {
        return entryId;
    }

    public void setEntryId(UUID entryId) {
        this.entryId = entryId;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }
}
