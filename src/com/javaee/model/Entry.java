package com.javaee.model;

import java.time.LocalDateTime;
import java.util.UUID;

public class Entry {
    private UUID id;
    private String description;
    private LocalDateTime deadline;
    private boolean done;
    private LocalDateTime createdAt;
    private int priority;
    private UUID userId;

    public Entry(String description, LocalDateTime deadline, boolean done, LocalDateTime createdAt, int priority, UUID userId) {
        this.id = UUID.randomUUID();
        this.description = description;
        this.deadline = deadline;
        this.done = done;
        this.createdAt = createdAt;
        this.priority = priority;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", deadline=" + deadline +
                ", done=" + done +
                ", createdAt=" + createdAt +
                ", priority=" + priority +
                ", userId=" + userId +
                '}';
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}

