<%@ page import="com.craftincode.ekj.advanced.model.User" %>
<%@ page import="com.craftincode.ekj.advanced.dao.UsersDAO" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="layout/css-imports.jsp" %>
</head>
<body id="page-top">
<%@ include file="layout/navbar.jsp" %>

<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Welcome to homepage</div>
        </div>
    </div>
</header>

<!-- Services -->
<section class="page-section" id="services">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center m-auto">
                <h1>Add new user</h1>


                <form action="add-user" method="post" novalidate="novalidate">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" data-validation-required-message="Please enter your login." placeholder="Your Login *" required="required" type="text" name="login">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" data-validation-required-message="Please enter your password." placeholder="Your Password *" required="required" type="text" name="password">
                                <p class="help-block text-danger"></p>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" data-validation-required-message="Please enter your name." id="name" placeholder="Your Name *" required="required" type="text" name="name">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" data-validation-required-message="Please enter your surname." placeholder="Your Surname *" required="required" type="text" name="surname">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" data-validation-required-message="Please enter your city." placeholder="Your City *" required="required" type="text" name="city">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" data-validation-required-message="Please enter your birth year." placeholder="Your Birth Year *" required="required" type="text" name="birthYear">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button class="btn btn-primary btn-xl text-uppercase" id="sendMessageButton" type="submit">Add new user</button>
                        </div>
                    </div>
                </form>

            </div>



        </div>
    </div>
</section>
<%@ include file="layout/footer.jsp" %>
</body>
</html>
