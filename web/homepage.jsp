<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="layout/css-imports.jsp" %>
</head>
<body id="page-top">
<%@ include file="layout/navbar.jsp" %>

<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Welcome to homepage</div>
        </div>
    </div>
</header>

<!-- Services -->
<section class="page-section" id="services">
    <div class="container">
        <h1>Users list</h1>

        <table>
            <tr>
                <th>Name</th>
            </tr>
        </table>
    </div>
</section>
<%@ include file="layout/footer.jsp" %>
</body>
</html>
