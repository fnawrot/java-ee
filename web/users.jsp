<%@ page import="com.javaee.model.User" %>
<%@ page import="com.javaee.dao.UsersDAO" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@ include file="layout/css-imports.jsp" %>
</head>
<body id="page-top">
<%@ include file="layout/navbar.jsp" %>

<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Welcome to homepage</div>
        </div>
    </div>
</header>

<!-- Services -->
<section class="page-section" id="services">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center col-md-8 m-auto">
                <h1>Users list</h1>

                <%
                    List<User> allUsers = (List<User>) request.getAttribute("users");

                %>

                <table class="table table-responsive table-striped">
                    <tr>
                        <th>ID</th>
                        <th>Login</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>City</th>
                        <th>Birth year</th>
                        <th>Action</th>
                    </tr>

                    <%
                        for (User user : allUsers) {
                    %>
                    <tr>
                        <td><%=user.getId()%>
                        </td>
                        <td><%=user.getLogin()%>
                        </td>
                        <td><%=user.getName()%>
                        </td>
                        <td><%=user.getSurname()%>
                        </td>
                        <td><%=user.getCity()%>
                        </td>
                        <td><%=user.getBirthYear()%>
                        </td>
                        <td>
                            <a
                                    class="btn btn-danger"
                                    href="delete-user?id=<%=user.getId()%>"
                            >
                                Delete
                            </a>
                        </td>
                    </tr>
                    <%
                        }
                    %>

                </table>

                <a href="add-user.jsp" class="btn btn-dark btn-lg">Add user</a>
            </div>



        </div>
    </div>
</section>
<%@ include file="layout/footer.jsp" %>
</body>
</html>
