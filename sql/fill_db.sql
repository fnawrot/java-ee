insert into entry values ('ff7b42ba-a825-4053-8e3e-52c5806f3a92', 'Garbage Collector', '2019-05-20', true, '2019-05-20', 1);
insert into entry values ('2ec26565-5f8b-4b05-ac27-f27fed0ba7b2', 'JIT compiler', '2019-05-30', false, '2019-05-31', 3);
insert into entry values ('4cb402b2-322a-471c-8850-ef1de81f044b', 'Funny cat', '2019-06-01', false, '2019-06-02', 1);
insert into entry values ('0178e1d2-1a53-4139-ad2d-eb811a2622d2', 'Funny Java program', '2019-07-01', false, '2019-07-02', 6);

insert into category values ('0b50d720-0089-48c3-ae9f-f05358488857', 'IT',null);
insert into category values ('deab00b6-4a07-42ad-aead-8d6828392501', 'Fun',null);
insert into category values ('c7ceb432-57e9-4fc0-ae8f-398849acaae7', 'Future',null);

insert into entry_category values ('ff7b42ba-a825-4053-8e3e-52c5806f3a92', '0b50d720-0089-48c3-ae9f-f05358488857');
insert into entry_category values ('2ec26565-5f8b-4b05-ac27-f27fed0ba7b2', '0b50d720-0089-48c3-ae9f-f05358488857');
insert into entry_category values ('4cb402b2-322a-471c-8850-ef1de81f044b', 'deab00b6-4a07-42ad-aead-8d6828392501');
insert into entry_category values ('0178e1d2-1a53-4139-ad2d-eb811a2622d2', 'deab00b6-4a07-42ad-aead-8d6828392501');
insert into entry_category values ('0178e1d2-1a53-4139-ad2d-eb811a2622d2', '0b50d720-0089-48c3-ae9f-f05358488857');
