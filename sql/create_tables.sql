create table jee_user (
  id uuid primary key,
  login varchar(50) not null,
  password varchar(50) not null,
  name varchar(50),
  surname varchar(50),
  city varchar(50),
  birth_year INT
);

create table category (
  id uuid primary key,
  name varchar(255),
  user_id uuid references jee_user(id)
);

create table entry (
  id uuid primary key,
  description varchar(255),
  deadline timestamp,
  done boolean,
  created_at timestamp,
  priority int,
  user_id uuid references jee_user(id)
);

create table entry_category (
  entry_id uuid references entry(id),
  category_id uuid references category(id),
  primary key (entry_id, category_id)
);